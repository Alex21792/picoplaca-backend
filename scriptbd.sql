--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.23
-- Dumped by pg_dump version 9.3.23
-- Started on 2019-05-27 21:12:11

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;

DROP DATABASE advance_latam;
--
-- TOC entry 1995 (class 1262 OID 33652)
-- Name: advance_latam; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE advance_latam WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Peru.1252' LC_CTYPE = 'Spanish_Peru.1252';


ALTER DATABASE advance_latam OWNER TO postgres;

\connect advance_latam

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 1 (class 3079 OID 11750)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 1998 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 176 (class 1259 OID 33678)
-- Name: auto; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE public.auto (
    aut_id bigint NOT NULL,
    aut_placa character varying NOT NULL,
    aut_chasis character varying(30) NOT NULL,
    aut_capacidad integer,
    aut_color bigint NOT NULL,
    aut_marca bigint NOT NULL,
    aut_modelo bigint NOT NULL
);


ALTER TABLE public.auto OWNER TO postgres;

--
-- TOC entry 1999 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN auto.aut_chasis; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.auto.aut_chasis IS 'numero de chasis del vehículo';


--
-- TOC entry 2000 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN auto.aut_capacidad; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.auto.aut_capacidad IS 'número de personas que caben en el auto';


--
-- TOC entry 175 (class 1259 OID 33676)
-- Name: auto_aut_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auto_aut_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auto_aut_id_seq OWNER TO postgres;

--
-- TOC entry 2001 (class 0 OID 0)
-- Dependencies: 175
-- Name: auto_aut_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auto_aut_id_seq OWNED BY public.auto.aut_id;


--
-- TOC entry 172 (class 1259 OID 33655)
-- Name: catalogo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE public.catalogo (
    ctg_id bigint NOT NULL,
    ctg_codigo character varying(6) NOT NULL,
    ctg_nombre character varying(30)
);


ALTER TABLE public.catalogo OWNER TO postgres;

--
-- TOC entry 2002 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN catalogo.ctg_codigo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.catalogo.ctg_codigo IS 'código personalizado para una rápida identificación del catálogo';


--
-- TOC entry 2003 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN catalogo.ctg_nombre; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.catalogo.ctg_nombre IS 'nombre o descricpión del catálogo';


--
-- TOC entry 171 (class 1259 OID 33653)
-- Name: catalogo_ctg_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.catalogo_ctg_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.catalogo_ctg_id_seq OWNER TO postgres;

--
-- TOC entry 2004 (class 0 OID 0)
-- Dependencies: 171
-- Name: catalogo_ctg_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.catalogo_ctg_id_seq OWNED BY public.catalogo.ctg_id;


--
-- TOC entry 174 (class 1259 OID 33665)
-- Name: catalogo_valor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE public.catalogo_valor (
    cvl_id bigint NOT NULL,
    cvl_descripcion character varying(30),
    ctg_id bigint
);


ALTER TABLE public.catalogo_valor OWNER TO postgres;

--
-- TOC entry 173 (class 1259 OID 33663)
-- Name: catalogo_valor_cvl_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.catalogo_valor_cvl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.catalogo_valor_cvl_id_seq OWNER TO postgres;

--
-- TOC entry 2005 (class 0 OID 0)
-- Dependencies: 173
-- Name: catalogo_valor_cvl_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.catalogo_valor_cvl_id_seq OWNED BY public.catalogo_valor.cvl_id;


--
-- TOC entry 178 (class 1259 OID 33706)
-- Name: reglamento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE public.reglamento (
    rgl_id bigint NOT NULL,
    rgl_dia bigint NOT NULL,
    rgl_hora_inicio time without time zone,
    rgl_hora_fin time without time zone,
    rgl_estado boolean DEFAULT true
);


ALTER TABLE public.reglamento OWNER TO postgres;

--
-- TOC entry 2006 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN reglamento.rgl_hora_inicio; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.reglamento.rgl_hora_inicio IS 'hora en que inicia el pico y placa';


--
-- TOC entry 2007 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN reglamento.rgl_estado; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.reglamento.rgl_estado IS 'estado para identificar si el reglamento(pico y placa) es activo o inactivo';


--
-- TOC entry 179 (class 1259 OID 33719)
-- Name: reglamento_detalles; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE public.reglamento_detalles (
    rgl_id bigint NOT NULL,
    rde_digito_placa integer NOT NULL,
    rde_estado boolean DEFAULT true
);


ALTER TABLE public.reglamento_detalles OWNER TO postgres;

--
-- TOC entry 2008 (class 0 OID 0)
-- Dependencies: 179
-- Name: COLUMN reglamento_detalles.rde_digito_placa; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.reglamento_detalles.rde_digito_placa IS 'último dígito de la placa';


--
-- TOC entry 177 (class 1259 OID 33704)
-- Name: reglamento_rgl_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.reglamento_rgl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reglamento_rgl_id_seq OWNER TO postgres;

--
-- TOC entry 2009 (class 0 OID 0)
-- Dependencies: 177
-- Name: reglamento_rgl_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.reglamento_rgl_id_seq OWNED BY public.reglamento.rgl_id;


--
-- TOC entry 1848 (class 2604 OID 33681)
-- Name: aut_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auto ALTER COLUMN aut_id SET DEFAULT nextval('public.auto_aut_id_seq'::regclass);


--
-- TOC entry 1846 (class 2604 OID 33658)
-- Name: ctg_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.catalogo ALTER COLUMN ctg_id SET DEFAULT nextval('public.catalogo_ctg_id_seq'::regclass);


--
-- TOC entry 1847 (class 2604 OID 33668)
-- Name: cvl_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.catalogo_valor ALTER COLUMN cvl_id SET DEFAULT nextval('public.catalogo_valor_cvl_id_seq'::regclass);


--
-- TOC entry 1849 (class 2604 OID 33709)
-- Name: rgl_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reglamento ALTER COLUMN rgl_id SET DEFAULT nextval('public.reglamento_rgl_id_seq'::regclass);


--
-- TOC entry 1986 (class 0 OID 33678)
-- Dependencies: 176
-- Data for Name: auto; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2010 (class 0 OID 0)
-- Dependencies: 175
-- Name: auto_aut_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auto_aut_id_seq', 1, true);


--
-- TOC entry 1982 (class 0 OID 33655)
-- Dependencies: 172
-- Data for Name: catalogo; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.catalogo (ctg_id, ctg_codigo, ctg_nombre) VALUES (1, 'DAY', 'Dias de la semana');
INSERT INTO public.catalogo (ctg_id, ctg_codigo, ctg_nombre) VALUES (2, 'MRC', 'Marca de vehículos');
INSERT INTO public.catalogo (ctg_id, ctg_codigo, ctg_nombre) VALUES (3, 'CLR', 'Colores');
INSERT INTO public.catalogo (ctg_id, ctg_codigo, ctg_nombre) VALUES (4, 'MDL', 'Modelos de vehículos');


--
-- TOC entry 2011 (class 0 OID 0)
-- Dependencies: 171
-- Name: catalogo_ctg_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.catalogo_ctg_id_seq', 6, true);


--
-- TOC entry 1984 (class 0 OID 33665)
-- Dependencies: 174
-- Data for Name: catalogo_valor; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.catalogo_valor (cvl_id, cvl_descripcion, ctg_id) VALUES (1, 'LUNES', 1);
INSERT INTO public.catalogo_valor (cvl_id, cvl_descripcion, ctg_id) VALUES (2, 'MARTES', 1);
INSERT INTO public.catalogo_valor (cvl_id, cvl_descripcion, ctg_id) VALUES (3, 'MIERCOLES', 1);
INSERT INTO public.catalogo_valor (cvl_id, cvl_descripcion, ctg_id) VALUES (4, 'JUEVES', 1);
INSERT INTO public.catalogo_valor (cvl_id, cvl_descripcion, ctg_id) VALUES (5, 'VIERNES', 1);
INSERT INTO public.catalogo_valor (cvl_id, cvl_descripcion, ctg_id) VALUES (6, 'SABADO', 1);
INSERT INTO public.catalogo_valor (cvl_id, cvl_descripcion, ctg_id) VALUES (7, 'DOMINGO', 1);
INSERT INTO public.catalogo_valor (cvl_id, cvl_descripcion, ctg_id) VALUES (8, 'CHEVROLET', 2);
INSERT INTO public.catalogo_valor (cvl_id, cvl_descripcion, ctg_id) VALUES (9, 'CITROEN', 2);
INSERT INTO public.catalogo_valor (cvl_id, cvl_descripcion, ctg_id) VALUES (10, 'FORD', 2);
INSERT INTO public.catalogo_valor (cvl_id, cvl_descripcion, ctg_id) VALUES (11, 'KIA', 2);
INSERT INTO public.catalogo_valor (cvl_id, cvl_descripcion, ctg_id) VALUES (12, 'NEGRO', 3);
INSERT INTO public.catalogo_valor (cvl_id, cvl_descripcion, ctg_id) VALUES (13, 'BLANCO', 3);
INSERT INTO public.catalogo_valor (cvl_id, cvl_descripcion, ctg_id) VALUES (14, 'VINO', 3);
INSERT INTO public.catalogo_valor (cvl_id, cvl_descripcion, ctg_id) VALUES (15, 'AZUL MARINO', 3);
INSERT INTO public.catalogo_valor (cvl_id, cvl_descripcion, ctg_id) VALUES (16, 'GRIS', 3);
INSERT INTO public.catalogo_valor (cvl_id, cvl_descripcion, ctg_id) VALUES (17, 'SUV', 4);
INSERT INTO public.catalogo_valor (cvl_id, cvl_descripcion, ctg_id) VALUES (18, 'CAMIONETA', 4);
INSERT INTO public.catalogo_valor (cvl_id, cvl_descripcion, ctg_id) VALUES (19, 'DEPORTIVO', 4);


--
-- TOC entry 2012 (class 0 OID 0)
-- Dependencies: 173
-- Name: catalogo_valor_cvl_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.catalogo_valor_cvl_id_seq', 19, true);


--
-- TOC entry 1988 (class 0 OID 33706)
-- Dependencies: 178
-- Data for Name: reglamento; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.reglamento (rgl_id, rgl_dia, rgl_hora_inicio, rgl_hora_fin, rgl_estado) VALUES (1, 1, '08:00:00', '19:00:00', true);
INSERT INTO public.reglamento (rgl_id, rgl_dia, rgl_hora_inicio, rgl_hora_fin, rgl_estado) VALUES (2, 2, '08:00:00', '19:00:00', true);
INSERT INTO public.reglamento (rgl_id, rgl_dia, rgl_hora_inicio, rgl_hora_fin, rgl_estado) VALUES (3, 3, '08:00:00', '19:00:00', true);
INSERT INTO public.reglamento (rgl_id, rgl_dia, rgl_hora_inicio, rgl_hora_fin, rgl_estado) VALUES (4, 4, '08:00:00', '19:00:00', true);
INSERT INTO public.reglamento (rgl_id, rgl_dia, rgl_hora_inicio, rgl_hora_fin, rgl_estado) VALUES (5, 5, '08:00:00', '19:00:00', true);


--
-- TOC entry 1989 (class 0 OID 33719)
-- Dependencies: 179
-- Data for Name: reglamento_detalles; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.reglamento_detalles (rgl_id, rde_digito_placa, rde_estado) VALUES (1, 0, true);
INSERT INTO public.reglamento_detalles (rgl_id, rde_digito_placa, rde_estado) VALUES (1, 1, true);
INSERT INTO public.reglamento_detalles (rgl_id, rde_digito_placa, rde_estado) VALUES (2, 2, true);
INSERT INTO public.reglamento_detalles (rgl_id, rde_digito_placa, rde_estado) VALUES (2, 3, true);
INSERT INTO public.reglamento_detalles (rgl_id, rde_digito_placa, rde_estado) VALUES (3, 4, true);
INSERT INTO public.reglamento_detalles (rgl_id, rde_digito_placa, rde_estado) VALUES (3, 5, true);
INSERT INTO public.reglamento_detalles (rgl_id, rde_digito_placa, rde_estado) VALUES (4, 6, true);
INSERT INTO public.reglamento_detalles (rgl_id, rde_digito_placa, rde_estado) VALUES (4, 7, true);
INSERT INTO public.reglamento_detalles (rgl_id, rde_digito_placa, rde_estado) VALUES (5, 8, true);
INSERT INTO public.reglamento_detalles (rgl_id, rde_digito_placa, rde_estado) VALUES (5, 9, true);


--
-- TOC entry 2013 (class 0 OID 0)
-- Dependencies: 177
-- Name: reglamento_rgl_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.reglamento_rgl_id_seq', 2, true);


--
-- TOC entry 1859 (class 2606 OID 33686)
-- Name: auto_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY public.auto
    ADD CONSTRAINT auto_pk PRIMARY KEY (aut_id);


--
-- TOC entry 1861 (class 2606 OID 33688)
-- Name: auto_placa_unique; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY public.auto
    ADD CONSTRAINT auto_placa_unique UNIQUE (aut_placa);


--
-- TOC entry 1853 (class 2606 OID 33662)
-- Name: catalogo_cod_unique; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY public.catalogo
    ADD CONSTRAINT catalogo_cod_unique UNIQUE (ctg_codigo);


--
-- TOC entry 1855 (class 2606 OID 33660)
-- Name: catalogo_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY public.catalogo
    ADD CONSTRAINT catalogo_pk PRIMARY KEY (ctg_id);


--
-- TOC entry 1857 (class 2606 OID 33670)
-- Name: catalogo_valor_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY public.catalogo_valor
    ADD CONSTRAINT catalogo_valor_id PRIMARY KEY (cvl_id);


--
-- TOC entry 1863 (class 2606 OID 33718)
-- Name: reglamento_dia_unique; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY public.reglamento
    ADD CONSTRAINT reglamento_dia_unique UNIQUE (rgl_dia);


--
-- TOC entry 1865 (class 2606 OID 33711)
-- Name: reglamento_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY public.reglamento
    ADD CONSTRAINT reglamento_pk PRIMARY KEY (rgl_id);


--
-- TOC entry 1867 (class 2606 OID 33723)
-- Name: reglamentos_detalles_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY public.reglamento_detalles
    ADD CONSTRAINT reglamentos_detalles_pk PRIMARY KEY (rgl_id, rde_digito_placa);


--
-- TOC entry 1869 (class 2606 OID 33689)
-- Name: auto_color_catalogovalor_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auto
    ADD CONSTRAINT auto_color_catalogovalor_fk FOREIGN KEY (aut_color) REFERENCES public.catalogo_valor(cvl_id);


--
-- TOC entry 1870 (class 2606 OID 33694)
-- Name: auto_marca_catalogovalor_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auto
    ADD CONSTRAINT auto_marca_catalogovalor_fk FOREIGN KEY (aut_marca) REFERENCES public.catalogo_valor(cvl_id);


--
-- TOC entry 1871 (class 2606 OID 33699)
-- Name: auto_modelo_catalogovalor_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auto
    ADD CONSTRAINT auto_modelo_catalogovalor_fk FOREIGN KEY (aut_modelo) REFERENCES public.catalogo_valor(cvl_id);


--
-- TOC entry 1868 (class 2606 OID 33671)
-- Name: catalogo_catalogovalor_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.catalogo_valor
    ADD CONSTRAINT catalogo_catalogovalor_fk FOREIGN KEY (ctg_id) REFERENCES public.catalogo(ctg_id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 1872 (class 2606 OID 33712)
-- Name: catalogovalor_reglamento_dia_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reglamento
    ADD CONSTRAINT catalogovalor_reglamento_dia_fk FOREIGN KEY (rgl_dia) REFERENCES public.catalogo_valor(cvl_id) ON DELETE SET NULL;


--
-- TOC entry 1873 (class 2606 OID 41844)
-- Name: fk94rp14d4tini6jqjotsc2bidq; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reglamento_detalles
    ADD CONSTRAINT fk94rp14d4tini6jqjotsc2bidq FOREIGN KEY (rgl_id) REFERENCES public.reglamento(rgl_id);


--
-- TOC entry 1997 (class 0 OID 0)
-- Dependencies: 6
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2019-05-27 21:12:11

--
-- PostgreSQL database dump complete
--

